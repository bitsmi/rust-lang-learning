//! Generate library docs for the enclosing item (main.rs file)
//! Multiline comments must include //! characters at the start of the line 
//! Library comments **also admits** `markdown syntax`
//! 

/// Generate library docs for the following item (main() function)
/// Multiline comments must include /// characters at the start of the line 
pub fn main() 
{
    // Inline comments

    /* Block comments
     * Multiline
     */
    print_message("World");
}

pub fn print_message(message:&str) 
{
    //! Generate library docs for the enclosing item (print_message function)
    println!("Hello, {}!", message);
}

/// Only public elements generate library docs. This function doesn't appear on documentation
fn private_function()
{

}