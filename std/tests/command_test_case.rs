use std::process::{ Command, Output, Child, ExitStatus };

/// Windows dependant
#[test]
fn simple_command_execution_test()
{
    let output:Output = Command::new("cmd")
            .arg("/C")
            .arg("echo hello")
            .output()
            .expect("Failed to execute command");

    let result:&str = std::str::from_utf8(output.stdout.as_slice()).unwrap();

    assert!(output.status.success());
    assert_eq!("hello\r\n", result);
}

/// Windows dependant
#[test]
fn command_spawn_test()
{
    let mut child:Child = Command::new("cmd")
            .arg("/C")
            .arg("echo hello")
            .spawn()
            .expect("Failed to execute command");

    let status:ExitStatus = child.wait().expect("Couldn't retrieve output");
    
    assert!(status.success());
}