# INSTALLATION

## Table of Contents

1. [Installation](#installation)
2. [Tools](#tools)
3. [Useful commands](#useful-commands)

## Installation

On windows, download and install Visual Studion Build Tools from <https://visualstudio.microsoft.com/es/downloads/>

Download `rustup-init.exe` from <https://www.rust-lang.org>
After installation all tools are installed to the `~/.cargo/` bin directory, and this is where you will find the Rust toolchain, including `rustc`, `cargo`, and `rustup`

#### Update installation

```bash
rustup update
```

#### Uninstall

```bash
rustup self uninstall
```

#### Check compiler version

```bash
rustc --version
```

## Tools

#### Visual Studio Code

* <https://marketplace.visualstudio.com/items?itemName=rust-lang.rust>

## Useful commands

#### Run local docs

```bash
rustup doc
```
