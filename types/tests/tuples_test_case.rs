#[test]
fn tuple_accessors_test() 
{
    let tup:(i32, f64, u8) = (500, 6.4, 1);
    // Tuple deconstruction into variables
    let (x, y, z) = tup;

    // Access through variables
    assert_eq!(y, 6.4);
    // Direct access
    assert_eq!(tup.0, 500);
}