#[test]
fn struct_accessors_test() 
{
    struct SampleStruct {
        field1: i32,
        field2: f64,
        field3: i32
    }

    let st1:SampleStruct = SampleStruct{field1: 500, field2: 6.4, field3:1};
     // Create struct with field init shorthand
    let field1:i32 = 500;
    let field2:f64 = 6.4;
    let field3:i32 = 1;
    let st2:SampleStruct = SampleStruct{field1, field2, field3};
    // Create struct with non ordered fields
    let st3:SampleStruct = SampleStruct{field2: 6.4, field3: 1, field1: 500};
    
    assert_eq!(st1.field1, 500);
    assert_eq!(st1.field2, 6.4);
    assert_eq!(st1.field3, 1);

    assert_eq!(st2.field1, 500);
    assert_eq!(st2.field2, 6.4);
    assert_eq!(st2.field3, 1);

    assert_eq!(st3.field1, 500);
    assert_eq!(st3.field2, 6.4);
    assert_eq!(st3.field3, 1);
}

#[test]
fn named_tuple_accessors_test() 
{
    struct SampleStruct(i32, f64, i32);

    let st1:SampleStruct = SampleStruct(500, 6.4, 1);

    assert_eq!(st1.0, 500);
    assert_eq!(st1.1, 6.4);
    assert_eq!(st1.2, 1);
}

#[test]
fn struct_update_test()
{
    struct Point {
        x:f64,
        y:f64,
        z:f64
    }

    let p1 = Point{x:1.0, y:10.0, z:20.0};
    let p2 = Point{x:1.5, ..p1};

    assert_eq!(p2.x, 1.5);
    assert_eq!(p2.y, 10.0);
    assert_eq!(p2.z, 20.0);
}