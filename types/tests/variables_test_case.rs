#[test]
fn immutable_variables_test()
{
    let x:i32 = 5;

    // Immutable variables cannot be re assigned -> Compiler error
//    x = 10;
}

#[test]
fn mutable_variables_test()
{
    let mut x:i32 = 5;
    let mut y = 10;

    x = 15;
    y = y + 10;

    assert_eq!(x, 15);
    assert_eq!(y, 20);
}

#[test]
fn shadowing_test()
{
    let x = 5;
    let mut y = 10;

    let x = 15;
    let y = y + 10;

    assert_eq!(x, 15);
    assert_eq!(y, 20);
}